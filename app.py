import cv2
import matplotlib.pyplot as plt
import numpy as np
from keras.preprocessing.image import img_to_array
from keras.models              import load_model

#######################################################################################

GLOBAL_CAM_INDEX = 0

def debug(image):
    if 1 == 1:
        cv2.imshow('debug',image)
        cv2.waitKey(0)

class FaceDetector():
    def __init__(self):
        detector_params = cv2.SimpleBlobDetector_Params()
        detector_params.filterByArea = True
        detector_params.maxArea = 1500
        emotion_model_path = 'models/_mini_XCEPTION.106-0.65.hdf5'
        self.emotion_classifier = load_model(emotion_model_path, compile=False)
        self.EMOTIONS = ["angry","disgust","scared", "happy", "sad", "surprised","neutral"]
        self.faceDet       = cv2.CascadeClassifier("opencv\data\haarcascades\haarcascade_frontalface_default.xml")
        self.faceDet_two   = cv2.CascadeClassifier("opencv\data\haarcascades\haarcascade_frontalface_alt2.xml")
        self.faceDet_three = cv2.CascadeClassifier("opencv\data\haarcascades\haarcascade_frontalface_alt.xml")
        self.faceDet_four  = cv2.CascadeClassifier("opencv\data\haarcascades\haarcascade_frontalface_alt_tree.xml")
        self.eyeDet        = cv2.CascadeClassifier("opencv\data\haarcascades\haarcascade_eye.xml")
        self.detector      = cv2.SimpleBlobDetector_create(detector_params)

    def detect_face(
        self,
        image,
        scale_factor=1.5,
        min_neighbors=3,
        min_size=(15,15)
    ):
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        facefeatures = None
        face       = self.faceDet.detectMultiScale(gray,       scaleFactor=scale_factor, minNeighbors=min_neighbors, minSize=min_size, flags=cv2.CASCADE_SCALE_IMAGE)
        face_two   = self.faceDet_two.detectMultiScale(gray,   scaleFactor=scale_factor, minNeighbors=min_neighbors, minSize=min_size, flags=cv2.CASCADE_SCALE_IMAGE)
        face_three = self.faceDet_three.detectMultiScale(gray, scaleFactor=scale_factor, minNeighbors=min_neighbors, minSize=min_size, flags=cv2.CASCADE_SCALE_IMAGE)
        face_four  = self.faceDet_four.detectMultiScale(gray,  scaleFactor=scale_factor, minNeighbors=min_neighbors, minSize=min_size, flags=cv2.CASCADE_SCALE_IMAGE)

        #  FIXME  need to remove faces that are contained within another face.
        if 0 < len(face):
            facefeatures = face
        if 0 < len(face_two):
            facefeatures = face_two
        if 0 < len(face_three):
            facefeatures = face_three
        if 0 < len(face_four):
            facefeatures = face_four
        return facefeatures

    def detect_eye(
        self,
        image,
        scale_factor=1.3,
        min_neighbors=3,
        min_size=(5,5)
    ):
        left_eye  = None
        right_eye = None
        gray_frame = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        eyes   = self.eyeDet.detectMultiScale(gray_frame, scaleFactor=scale_factor, minNeighbors=min_neighbors, minSize=min_size, flags=cv2.CASCADE_SCALE_IMAGE) # detect eyes
        width  = np.size(image, 1)
        height = np.size(image, 0)
        for (x, y, w, h) in eyes:
            if y > height / 2:  # NOTE this was detected, but invalid data
                pass
            eyecenter = x + w / 2   # NOTE  eye center
            if eyecenter < width * 0.5:
                left_eye = image[y:y + h, x:x + w]
            else:
                right_eye = image[y:y + h, x:x + w]
        return left_eye, right_eye

    def detect_emotion(self, image):
        gray  = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        roi   = cv2.resize(gray, (48, 48))
        roi   = roi.astype("float") / 255.0
        roi   = img_to_array(roi)
        roi   = np.expand_dims(roi, axis=0)
        preds = self.emotion_classifier.predict(roi)[0]
        emotion_probability = np.max(preds)
        label = self.EMOTIONS[preds.argmax()]
        return label, emotion_probability


#######################################################################################

fd=FaceDetector()

def detect_face(image):
    faces=fd.detect_face(image)
    try:
        index = 1
        if faces is not None:
            for face in faces:
                x, y, w, h = face
                         
                cv2.rectangle(
                    image,
                    (x,y),
                    (x+w, y+h),
                    (127, 255,0),
                    3
                )
                
                emo, emo_prob = fd.detect_emotion(image[y:y+h, x:x+w])

                text = "Face{index:n}:\n\n{emotion:s}\n%{prop:2.2f}".format(
                    index=index, 
                    emotion=emo,
                    prop=emo_prob * 100
                )

                for i, line in enumerate(text.split('\n')):
                    y = y + 30
                    cv2.putText(
                        image,
                        line,
                        (x+w+5, y+5),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1,
                        (127, 255,0),
                        1,
                        cv2.LINE_AA
                    )
                index += 1
    except Exception as e:
        print(e)
    return image

def detect_eyes(image):  #  FIXME  this is not working.
    eyes = fd.detect_eye(
                    image[y:y+h, x:x+w],
                    scale_factor=1.9,
                    min_neighbors=3,
                    min_size=(5,5)
    )
    if eyes is not None:
        for eye in eyes:
            if eye is not None:
                eye       = fd.cut_eyebrows(eye)
                fd.blob_process(eye)
                eye       = cv2.drawKeypoints(
                    eye,
                    keypoints,
                    eye,
                    (0, 0, 255),
                    cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS
                )

def Live():
    try:
        cap = cv2.VideoCapture(GLOBAL_CAM_INDEX)
        cap.set(cv2.CAP_PROP_AUTO_EXPOSURE,True)
        cap.set(cv2.CAP_PROP_AUTOFOCUS,100)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 768)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1024)
    except Exception as e:
        print(e)
        return -1
    while True:
        ret, frame = cap.read()
        if ret:
            cv2.namedWindow("frame",cv2.WINDOW_FULLSCREEN)
            cv2.imshow('frame', detect_face(frame))
            key = cv2.waitKey(1)
            if key == 27:
                break
    cv2.destroyAllWindows()

if __name__ == '__main__':
    Live()
